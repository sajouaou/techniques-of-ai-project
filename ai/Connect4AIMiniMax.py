import math
from random import shuffle

from game.boardFunction import *
from game.gameplay import *


class miniMaxAI(StupidPlayer):
    """
    Class which implements miniMax algotihm
    """

    def __init__(self, depth, name="miniMax AI"):
        """
        Constructor of the AI
        :param depth: Depth of the miniMax AI
        :param name: name of the aI
        """
        super(miniMaxAI, self).__init__(name)
        self.depth = depth
        self.alpha, self.beta = -math.inf, math.inf
        self.maximizingScore = True

    def maximize(self, valid_positions, alpha, beta, game, board, depth):
        """
        Maximize step
        :param valid_positions: list of all valid possible positions to play
        :param alpha : alpha
        :param beta : beta
        :param game: game state
        :param board: board
        :param depth: depth
        """
        value = -math.inf
        column = random.choice(valid_positions)
        for col in valid_positions:
            row = get_next_row(game, board, col)
            b_copy = board.copy()
            b_copy[col][row] = self.turn

            new_score = self.min_max(b_copy, alpha, beta, game, depth - 1, False)[1]

            if new_score > value:  # Maximum between new_score and current best score
                value = new_score
                column = col

            alpha = max(alpha, value)  # Alpha-beta pruning
            if alpha >= beta:
                break

        return column, value

    def minimize(self, valid_positions, alpha, beta, game, board, depth):
        """
        Minimize step
        :param valid_positions: list of all valid possible positions to play
        :param alpha : alpha
        :param beta : beta
        :param game: game state
        :param board: board
        :param depth: depth
        """
        value = math.inf
        column = random.choice(valid_positions)
        for col in valid_positions:
            row = get_next_row(game, board, col)
            b_copy = board.copy()
            b_copy[col][row] = 3 - self.turn
            new_score = self.min_max(b_copy, alpha, beta, game, depth - 1, True)[1]

            if new_score < value:  # Minimum between new_score and current best score
                value = new_score
                column = col

            beta = min(beta, value)
            if beta <= alpha:  # Alpha-beta pruning
                break

        return column, value

    def min_max(self, board, alpha, beta, game, depth, maximizingScore):
        """
        MiniMax Strategy with alpha-beta pruning and given depth
        :param board: board
        :param alpha : alpha
        :param beta : beta
        :param game: game state
        :param depth: depth
        :param maximizingScore: if need to maximize or minimize
        """
        valid_positions = get_valid_positions(game, board)
        isAIWin = game.check_state_win(board, self.turn)
        isOtherWin = game.check_state_win(board, 3 - self.turn)
        isEnd = isAIWin or isOtherWin or len(valid_positions) == 0

        # Return score if it is the end
        if isEnd or depth == 0:
            if isAIWin and depth == self.depth - 1:
                return (None, 100000000000)
            return (None, compute_score(board, self.turn))

        shuffle(valid_positions)
        if maximizingScore:
            return self.maximize(valid_positions, alpha, beta, game, board, depth)
        else:  # Minimizing player
            return self.minimize(valid_positions, alpha, beta, game, board, depth)

    def ask_position(self, game):
        """
        ask position if needed
        :param game: game
        """
        column, value = self.min_max(game.get_board(), self.alpha, self.beta, game, self.depth, True)
        return column
