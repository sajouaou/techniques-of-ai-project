import numpy as np

from ai.Connect4AIMiniMax import *
from random import random, randint
from game.boardFunction import get_valid_positions
from game.gameplay import StupidPlayer, GamePlay, Connect4Viewer, pygame, HumanPlayer, DRAW, WIN


class connect4Agent(StupidPlayer):

    def __init__(self, epsilon=0.2, tau=0.5, name="Agent", choosefunction=None):
        self._won = None
        self.epsilon = epsilon
        self.turn = 0
        self.name = name
        self.begin = True

        self.alpha = 0.3
        self.gamma = 0.4

        self.tau = tau
        self.choosefunction = self.choose_greedy if choosefunction == None else choosefunction
        self.q = {}

        self.win_reward = 1
        self.lose_reward = -2
        self.draw_reward = 0.5

    def ask_position(self, game):
        valid_positions = get_valid_positions(game, game.get_board())
        self.position = self.choosefunction(game, game.get_board(), valid_positions)
        return self.position

    def afterplacing(self, game):
        self.learn(game.get_board(), get_valid_positions(game, game.get_board()), self.position, game)

    def choose_greedy(self, game, board, valid_positions):
        """
        choose according to e-greedy function
        :param board: board
        :param valid_positions: all valid possible positions
        :return: a position to play
        """
        actual_state = boardToHashable(self, board)
        if self.begin:
            self.begin = False
            return self.chooseRandom(valid_positions)
        else:
            if random() < self.epsilon:
                # case epsilon = exploration
                return self.chooseRandom(valid_positions)
            else:
                # other case
                return self.get_best_position(actual_state, valid_positions)

    def get_best_position(self, actual_state, valid_positions):
        """
        get best position
        :param actual_state: actual state
        :param valid_positions: valid possible positions
        :return: best position
        """
        rewards = [self.getQ(actual_state, pos) for pos in valid_positions]
        maximum = max(rewards)
        ind = np.where(rewards == maximum)[0]

        return valid_positions[ind[randint(0, len(ind) - 1)] if len(ind) > 0 else randint(0, len(valid_positions) - 1)]

    def chooseRandom(self, valid_positions):
        """
        return a random position among valid
        :param valid_positions: valid possible positions
        :return: random position
        """
        pos = np.random.choice(valid_positions)
        return pos

    def getQ(self, state, pos):
        if self.q.get((state, pos)) is None:
            self.q[(state, pos)] = 1.0
        return self.q.get((state, pos))

    def learn(self, board, positions, prev_pos, game):
        """
        Q-Learn algorithm
        """
        reward = 0
        if (self._won == None):
            if self._won == DRAW:
                reward = self.draw_reward
            elif self._won == WIN:
                reward = self.win_reward
            else:
                reward = self.lose_reward

        old_state = boardToHashable(self, game.oldboard)
        prev = self.getQ(old_state, prev_pos)
        if (len(positions) > 0):
            maxqnew = max([self.getQ(boardToHashable(self, board), pos) for pos in positions])
            self.q[(old_state, prev_pos)] = prev + self.alpha * ((reward + self.gamma * maxqnew) - prev)


def boardToHashable(player, board):
    """
    Convert a board (list of list) into a hashable version
    :param player: player
    :param board: board
    :return: state
    """
    mes = "1"
    for i in range(len(board)):
        for j in range(len(board[i])):
            if board[i][j] == 0:
                mes += str("0")
            else:
                if board[i][j] == player.turn:  # save state with
                    mes += str("1")
                else:
                    mes += str("2")
    state = int(mes)
    return state


def train(agent, opponent, nbGames=10000, trials=50,viewable=False):
    """
    Training algorithm (run a number of games)
    :param agent: agent (AI)
    :param opponent: opponent
    :param nbGames: number of games to play
    :param trials: number of trials
    """
    game = GamePlay(agent, opponent)
    if viewable:
        view = Connect4Viewer(game=game)
        view.initialize()
    count = np.array([np.array([0, 0, 0]) for i in range(trials)])

    for i in range(trials):
        game.swapPlayer()
        for j in range(nbGames):
            game.reset_game()
            game.run()
            winner = "Nobody"
            if agent._won == 1:
                winner = agent.name
            elif opponent._won == 1:
                winner = opponent.name
            print("WIN : ", winner)
            count[i][agent._won + 1] += 1
        print(count[i])
        print("Victoires : ", count[i][2] * 100 / sum(count[i]), "%")
        print("Défaites : ", count[i][0] * 100 / sum(count[i]), "%")
        print("Egalité : ", count[i][1] * 100 / sum(count[i]), "%")