#import pickle
import time

import joblib
from ai.Connect4AIMiniMax import StupidPlayer, miniMaxAI
from ai.connect4Agent import connect4Agent, train


def save_object(ai, filename="my_ai.pkl"):
    joblib.dump(ai,filename)


def load_object(filename="my_ai.pkl"):
    return joblib.load(filename)