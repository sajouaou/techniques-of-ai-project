"""

authors:
Ajouaou Soufiane 000459811
Belgada Wassim 000459836
El Achouchi Iliass 000462349
Nidhal Mareghni 000459346


"""
from ai import Serialization
from ai.Connect4AIMiniMax import miniMaxAI
from ai.connect4Agent import train, connect4Agent
from game.gameplay import *

if __name__ == '__main__':

    """
    User manual :
    If you want to use of the AI or yourself, please change the lines 40 and 41
    by what you want
    
    Minimax : miniMaxAI(depth)
    You : HumanPlayer()
    
    If you want to use the Q Learning Agent, replace by 
    Q Learning Agent : connect4Agent()
    and uncomment the line 43 for training.
    To use your trained IA, use the AI at line 42. Make sure to save it after with line 46.
    
    Make sure to change the number of games and trials if you want to.
    If you want to see the games in the training, add the parameter viewable=True in 
    the train function call.
    
    """
    print("-----------------------------------------------")
    print("Beginning of the game")
    print("-----------------------------------------------")

    player1 = HumanPlayer()
    player2 = miniMaxAI(2)
    #saved_player = Serialization.load_object()
    #train(player1, player2, trials=1, nbGames=10,viewable=True) # you can add at last parameter viewable=True
    #Serialization.save_object(saved_player)
    game = GamePlay(player1, player2)
    game.reset_game()

    view = Connect4Viewer(game=game)
    view.initialize()
    game.run()
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
    pygame.quit()
