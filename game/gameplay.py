from time import sleep

import numpy as np

from game.connectfour.game import *

WIN = 1
LOSE = -1
DRAW = 0


class GamePlay(Connect4Game):

    def __init__(self, player1, player2, rows=6, cols=7):
        self.player1 = player1
        self.player2 = player2
        super(GamePlay, self).__init__(rows, cols)
        self.player1.turn = 1
        self.player2.turn = 2
        self.compteur = 0
        self.max = self.get_rows() * self.get_cols()

        self.oldboard = None

    def swapPlayer(self):
        temp = self.player1
        self.player1 = self.player2
        self.player2 = temp

        self.player1.turn = 1
        self.player2.turn = 2

    def place(self, c, player):
        """
            Tries to place the playing colour on the cth column
            :param c: column to place on
            :return: position of placed colour or None if not placeable
        """
        for r in range(self._rows):
            if self._board[c][r] == 0:
                self.oldboard = self._board.copy()
                self._board[c][r] = player.turn
                self.compteur += 1
                print(player.name+" played: "+ str(c+1)+ " " + str(r+1) + "   (col,row)")
                print("")

                self.notify(Event.PIECE_PLACED, (c, r))
                return c, r
        return None

    def update_win_stat(self, pos, playerActive, playerIdle):
        player = playerActive.turn
        win = self.check_win(self._board, pos, player)
        if win == WIN:
            self.notify(Event.GAME_WON, player)
            self._won = player
            playerActive._won = WIN
            playerIdle._won = LOSE
        elif win == DRAW:
            self._won = 0
            playerActive._won = DRAW
            playerIdle._won = DRAW

    def check_state_win(self, board, player):
        for i in range(len(board)):
            for j in range(len(board[i])):
                pos = (i, j)
                if self.check_win(board, pos, player):
                    return True
        return False

    def check_win(self, board, pos, player):
        c = pos[0]
        r = pos[1]
        min_col = max(c - 3, 0)
        max_col = min(c + 3, self._cols - 1)
        min_row = max(r - 3, 0)
        max_row = min(r + 3, self._rows - 1)

        # Horizontal check
        count = 0
        for ci in range(min_col, max_col + 1):
            if board[ci][r] == player:
                count += 1
            else:
                count = 0
            if count == 4:
                return WIN

        # Vertical check
        count = 0
        for ri in range(min_row, max_row + 1):
            if board[c][ri] == player:
                count += 1
            else:
                count = 0
            if count == 4:
                return WIN

        count1 = 0
        count2 = 0
        # Diagonal check
        for i in range(-3, 4):
            # bottom-left -> top-right
            if 0 <= c + i < self._cols and 0 <= r + i < self._rows:
                if board[c + i][r + i] == player:
                    count1 += 1
                else:
                    count1 = 0
                if count1 == 4:
                    return WIN
            # bottom-right -> top-let
            if 0 <= c + i < self._cols and 0 <= r - i < self._rows:
                if board[c + i][r - i] == player:
                    count2 += 1
                else:
                    count2 = 0
                if count2 == 4:
                    return WIN

        if self.compteur == self.max:
            return DRAW

    def reset_game(self):
        super(GamePlay, self).reset_game()
        self.compteur = 0
        self.player1._won = None
        self.player2._won = None
        self._board = np.array([np.array([0 for _ in range(self._rows)]) for _ in range(self._cols)])

    def run(self):
        self.reset_game()
        while self._won == None:
            self.askPlayer(self.player1, self.player2)
            if self._won == None:
                self.askPlayer(self.player2, self.player1)

    def askPlayer(self, playerActive, playerIdle):
        pos = None
        while pos == None:
            pos = playerActive.ask_position(self)
            pos = self.place(pos, playerActive)
        playerActive.afterplacing(self)
        self.update_win_stat(pos, playerActive, playerIdle)


class StupidPlayer():
    def __init__(self, name="Random Player"):
        self._won = None
        self.turn = 0
        self.name = name

    def ask_position(self, game):
        return random.randint(0, game.get_cols() - 1)

    def afterplacing(self, game):
        pass


class HumanPlayer(StupidPlayer):
    def __init__(self, name="Human Player"):
        super(HumanPlayer, self).__init__(name)

    def ask_position(self, game):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                    return (pygame.mouse.get_pos()[0] // SQUARE_SIZE)
