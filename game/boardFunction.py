def get_valid_positions(game, board):
    """
    return valid possible positions
    :param game: game
    :param board: board
    :return: the valid possible positions
    """
    valid_positions = []
    for col in range(game.get_cols()):
        if is_valid_position(col, game, board):
            valid_positions.append(col)
    return valid_positions


def is_valid_position(col, game, board):
    """
    return true if is a valid position
    :param col:
    :param game:
    :param board:
    :return: true if it is a correct position
    """
    for row in range(game.get_rows()):
        if board[col][row] == 0:
            return True
    return False


def get_next_row(game, board, column):
    """
    Gets the next open row
    :param game: game
    :param board: board
    :param column: column
    """
    for row in range(game.get_rows()):
        if board[column][row] == 0:
            return row


def compute_horizontal_score(row_count, column_count, window_length, board, piece):
    """
    Compute horizontal score
    :param row_count: number of rows
    :param column_count: number of columns
    :param window_length: size of the window
    :param board: board
    :param piece: value of the current piece
    """
    score = 0
    for r in range(row_count):
        for c in range(column_count - 3):
            window = [board[c + i][r] for i in range(window_length)]
            score += window_score(window, piece)

    return score


def compute_vertical_score(row_count, column_count, window_length, board, piece):
    """
    Compute vertical score
    :param row_count: number of rows
    :param column_count: number of columns
    :param window_length: size of the window
    :param board: board
    :param piece: value of the current piece
    """
    score = 0
    for c in range(column_count):
        for r in range(row_count - 3):
            window = [board[c][r + i] for i in range(window_length)]
            score += window_score(window, piece)

    return score


def compute_ascending_diagonal_score(row_count, column_count, window_length, board, piece):
    """
    Compute ascending diagonal score
    :param row_count: number of rows
    :param column_count: number of columns
    :param window_length: size of the window
    :param board: board
    :param piece: value of the current piece
    """
    score = 0
    for r in range(row_count - 3):
        for c in range(column_count - 3):
            window = [board[c + i][r + i] for i in range(window_length)]
            score += window_score(window, piece)

    return score


def compute_descending_diagonal_score(row_count, column_count, window_length, board, piece):
    """
    Compute descending diagonal score
    :param row_count: number of rows
    :param column_count: number of columns
    :param window_length: size of the window
    :param board: board
    :param piece: value of the current piece
    """
    score = 0
    for r in range(row_count - 3):
        for c in range(column_count - 3):
            window = [board[c + i][r + 3 - i] for i in range(window_length)]
            score += window_score(window, piece)

    return score


def compute_score(board, piece):
    """
    Compute score with the current state
    :param board: board
    :param piece: value of the current piece
    """
    score = 0

    ROW_COUNT = len(board[0])
    COLUMN_COUNT = len(board)
    WINDOW_LENGTH = 4
    score += compute_horizontal_score(ROW_COUNT, COLUMN_COUNT, WINDOW_LENGTH, board, piece)

    score += compute_vertical_score(ROW_COUNT, COLUMN_COUNT, WINDOW_LENGTH, board, piece)

    score += compute_ascending_diagonal_score(ROW_COUNT, COLUMN_COUNT, WINDOW_LENGTH, board, piece)

    score += compute_descending_diagonal_score(ROW_COUNT, COLUMN_COUNT, WINDOW_LENGTH, board, piece)

    return score


def window_score(window, piece):
    """
    Give a score for windows according to the current state and given piece
    :param window: calculated window
    :param piece: value of the current piece
    """
    score = 0
    opp_piece = 3 - piece

    nbPiece = window.count(piece)
    noPiece = window.count(0)
    nbOppPiece = window.count(opp_piece)

    # Positive streaks
    if nbPiece == 4:
        score += 1000
    elif nbPiece == 3 and noPiece == 1:
        score += 100
    elif nbPiece == 2 and noPiece == 2:
        score += 10

    # Negative streaks
    if nbOppPiece == 4:
        score -= 1000000
    elif nbOppPiece == 3 and noPiece == 1:
        score -= 1000
    elif nbOppPiece == 2 and noPiece == 2:
        score -= 100
    elif nbOppPiece == 2 and nbPiece == 1:
        score += 10
    elif nbOppPiece == 3 and nbPiece == 1:
        score += 100

    return score
